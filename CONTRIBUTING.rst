.. highlight:: shell

============
Contributing
============

Contributions are welcome, and they are greatly appreciated! Every little bit
helps, and credit will always be given.

Repository:  https://framagit.org/numenic/femap_neutral_parser
