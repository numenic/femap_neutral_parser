# -*- coding: utf-8 -*-

"""Top-level package for FEMAP neutral Parser."""

__author__ = """Nicolas Cordier"""
__email__ = "nicolas.cordier@numeric-gmbh.ch"
__version__ = "0.14.0"

from .parser import Parser
